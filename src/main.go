package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
	"time"

	"github.com/gen2brain/beeep"
)

func main() {
	go verify_kernel()
	verify_systemd()
}

func verify_kernel() {
	fmt.Println("salut")
	_, kernel := os.Stat("/var/lib/pkg/DB/kernel/META")
	if kernel != nil {
		resp, _ := exec.Command("uname", "-r").Output()
		string_resp := string(resp)
		to_len := strings.Split(string_resp, ".")
		release := to_len[len(to_len)-2]
		head := to_len[len(to_len)-3]
		kernel_db := fmt.Sprintf("/var/lib/pkg/DB/kernel-%s%s/META", head, release)
		fmt.Println(kernel_db)
		resp_kernel, _ := ioutil.ReadFile(kernel_db)
		kernel_string := string(resp_kernel)
		time.Sleep(600 * time.Second)
		resp_update, _ := ioutil.ReadFile(kernel_db)
		update := string(resp_update)
		if kernel_string != update {
			beeep.Alert("Reboot", "Your current kernel has been updated", "none")
		}

	} else {
		resp, _ := ioutil.ReadFile("/var/lib/pkg/DB/kernel/META")
		kernel_string := string(resp)
		time.Sleep(600 * time.Second)
		resp_update, _ := ioutil.ReadFile("/var/lib/pkg/DB/kernel/META")
		update := string(resp_update)
		if kernel_string != update {
			beeep.Alert("Reboot", "Your current kernel has been updated", "none")
		}
	}
}

func verify_systemd() {
	fmt.Println("salut")
	systemd, _ := ioutil.ReadFile("/var/lib/pkg/DB/systemd/META")
	time.Sleep(600 * time.Second)
	systemd_update, _ := ioutil.ReadFile("/var/lib/pkg/DB/systemd/META")
	systemd_verif := string(systemd)
	systemd_verif_update := string(systemd_update)
	if systemd_verif != systemd_verif_update {
		beeep.Alert("Reboot", "Systemd has been updated", "none")
	}
}
